import { Fragment, useEffect, useState } from 'react';
import ProductCard from '../components/ProductCard';
//import productsData from '../data/productsData';

export default function Products(){
	// console.log(productsData);
	// console.log(productsData[0]);
	
	// State that will be used to store the products retrieved from the database
	const [products, setProducts] = useState([]);

	/*useEffect(() => {
		fetch('http://localhost:4000/courses/all')
		.then(res => res.json())
		.then(data => {
			console.log(data);

			// Sets the "products" state to map the data retrieved from the fetch request in several "ProductCard" components
			setProducts(data.map(product => {
					return(
						<ProductCard key={product.id} productProp={product} />
					)
				})
			);	
		})
	}, [])*/

	
	return (		
		<Fragment>
			{products}
		</Fragment>
	)}