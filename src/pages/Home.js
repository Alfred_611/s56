import { Fragment } from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
//import CourseCard from '../components/CourseCard';

export default function Home(){
	const data={
		title: "CJ Music Shop",
		content: "Shop for high quality musical instruments now!",
		destination: "/products",
		label: "View all products"
	}

	return(
		<Fragment>
			<Banner data={data}/>
			<Highlights />
		</Fragment>
	)
}