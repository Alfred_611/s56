import { Row, Col, Card, Image } from 'react-bootstrap';
export default function Admin(){
		
	return (

		<Row className="mt-4 mb-3">
		<Col xs={12} md={12} className="mt-4 mb-3">
		<h1 className="mt-4 mb-4">Administrator Dashboard</h1>
		</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-4">
				  
				  <Card.Body>
				    <Card.Title>
				    <h3>User Database</h3>
				    </Card.Title>
				    <Card.Text>
				      Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				      tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				      quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				      consequat.
				    </Card.Text>
				    
				  </Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-4">
				  
				  <Card.Body>
				    <Card.Title>
				    <h3>Product Database</h3>
				    </Card.Title>
				    <Card.Text>
				      Duis aute irure dolor in reprehenderit in voluptate velit esse
				      cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				      proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
				    </Card.Text>
				    	
				  </Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-4">
				  
				  <Card.Body>
				    <Card.Title>
				    <h3>Add Product</h3>
				    </Card.Title>
				    <Card.Text>
				      Duis aute irure dolor in reprehenderit in voluptate velit esse
				      cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				      proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
				    </Card.Text>
				    
				  </Card.Body>
				</Card>
			</Col>
		</Row>
	)
}