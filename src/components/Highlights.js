import { Row, Col, Card, Image } from 'react-bootstrap';
export default function Highlights(){
	return (




		<Row className="mt-2 mb-3">
			<Col xs={12} md={3}>
			<Image src="https://images.unsplash.com/photo-1550590774-adc439e48ac6?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1587&q=80" fluid /> 
				<Card className="cardHighlight p-2">
				  
				  <Card.Body>
				    <Card.Title>
				    <h4>Guitars</h4>
				    </Card.Title>
				    <Card.Text>
				      Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				      tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				      quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				      consequat.
				    </Card.Text>
				    
				  </Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={3}>
			<Image src="https://images.unsplash.com/photo-1608663003920-757dd225d6c5?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1587&q=80" fluid />
				<Card className="cardHighlight p-2">
				  
				  <Card.Body>
				    <Card.Title>
				    <h4>Percussion</h4>
				    </Card.Title>
				    <Card.Text>
				      Duis aute irure dolor in reprehenderit in voluptate velit esse
				      cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				      proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
				    </Card.Text>
				    	
				  </Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={3}>
			<Image src="https://images.unsplash.com/photo-1599494009395-5b43c783a2d5?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1587&q=80" fluid />
				<Card className="cardHighlight p-2">
				  
				  <Card.Body>
				    <Card.Title>
				    <h4>Keyboards</h4>
				    </Card.Title>
				    <Card.Text>
				      Duis aute irure dolor in reprehenderit in voluptate velit esse
				      cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				      proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
				    </Card.Text>
				    
				  </Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={3}>
			<Image src="https://images.unsplash.com/photo-1590571054052-eec7ecae1f05?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=687&q=80" fluid />
				<Card className="cardHighlight p-2">
				  
				  <Card.Body>
				    <Card.Title>
				    <h4>Others</h4>
				    </Card.Title>
				    <Card.Text>
				      Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				      tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				      quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				      consequat.
				    </Card.Text>
				    
				  </Card.Body>
				</Card>
			</Col>
		</Row>
	)
}